<?php

namespace ffsoft\fx\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * Class FxRatesSource
 *
 * @package ffsoft\fx\enums
 */
class FxRatesSource extends BaseEnum
{
    /**
     * cnb.cz
     */
    const CNB = 1;
    /**
     * kurzy.cz
     */
    const KURZY = 2;
    /**
     * kurzy.cz
     */
    const CURRENCY_API = 3;
    /**
     * @var array
     */
    public static $list
        = [
            self::CNB   => 'CNB',
            self::KURZY => 'Kurzy.cz',
            self::CURRENCY_API => 'Currency API',
        ];
    /**
     * @var string message category
     */
    public static $messageCategory = 'common';
}