<?php

declare(strict_types=1);

namespace ffsoft\fx\exceptions;

interface ExtraException
{
    /**
     * Extra data about exception
     * @return array
     */
    public function getExtra(): array;
}
