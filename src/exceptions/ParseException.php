<?php

declare(strict_types=1);

namespace ffsoft\fx\exceptions;

use Exception;
use Throwable;

class ParseException extends Exception implements ExtraException
{
    /** @var array */
    protected $extra;

    public function __construct(
        array $extra = [],
        Throwable $previous = null,
        string $message = 'Exception during the parsing process',
        int $code = 0
    ) {
        parent::__construct($message, $code, $previous);
        $this->extra = $extra;
    }

    public function getExtra(): array
    {
        return $this->extra;
    }
}
