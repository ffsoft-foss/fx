<?php

namespace ffsoft\fx\models;

use common\models\Currencies;
use common\models\enums\RateTypes;
use common\traits\Validators;
use ffsoft\fx\enums\FxRatesSource;
use Yii;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "fx_rates".
 *
 * @property int        $id
 * @property int        $currency_id_in
 * @property int        $currency_id_out
 * @property float      $rate
 * @property int        $source
 * @property int        $type
 * @property int        $created_at
 * @property int        $updated_at
 * @property int        $deleted_at
 *
 * @property Currencies $currencyIn
 * @property Currencies $currencyOut
 */
class FxRates extends ActiveRecord
{
    use Validators;
    const EVENT_AFTER_CREATE = 'afterCreate';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fx_rates';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('common', 'ID'),
            'currency_id_in'  => Yii::t('common', 'Currency Id In'),
            'currency_id_out' => Yii::t('common', 'Currency Id Out'),
            'rate'            => Yii::t('common', 'Rate'),
            'source'          => Yii::t('common', 'Source'),
            'type'            => Yii::t('common', 'Type'),
            'created_at'      => Yii::t('common', 'Created At'),
            'updated_at'      => Yii::t('common', 'Updated At'),
            'deleted_at'      => Yii::t('common', 'Deleted At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestampBehavior'  => [
                'class'      => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_VALIDATE => ['created_at'],
                ],
                'value'      => function () {
                    if (empty($this->created_at)) {
                        return time();
                    }

                    return $this->created_at;
                },
            ],
            [
                'class'      => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_VALIDATE => ['updated_at'],
                ],
                'value'      => function () {
                    return time();
                },
            ],
            'softDeleteBehavior' => [
                'class'                     => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_at' => time(),
                    'updated_at' => time(),
                ],
                'replaceRegularDelete'      => true,
            ],
        ];
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return int
     */
    public function getCurrencyIdIn(): int
    {
        return $this->currency_id_in;
    }

    /**
     * @return int
     */
    public function getCurrencyIdOut(): int
    {
        return $this->currency_id_out;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyIn()
    {
        return $this->hasOne(Currencies::class, ['id' => 'currency_id_in'])
            ->andWhere(['currencies.deleted_at' => null]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyOut()
    {
        return $this->hasOne(Currencies::class, ['id' => 'currency_id_out'])
            ->andWhere(['currencies.deleted_at' => null]);
    }

    /**
     * @return int
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @return int
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = $this->getRules();

        $rules['CurrencyIdInValidator'] = [
            'currency_id_in',
            'exist',
            'skipOnError'     => true,
            'targetClass'     => Currencies::class,
            'targetAttribute' => ['currency_id_in' => 'id'],
        ];

        $rules['CurrencyIdOutValidator'] = [
            'currency_id_out',
            'exist',
            'skipOnError'     => true,
            'targetClass'     => Currencies::class,
            'targetAttribute' => ['currency_id_out' => 'id'],
        ];

        $rules['SourceIn'] = [
            'source',
            'in',
            'range' => FxRatesSource::getConstantsByName(),
        ];

        $rules['TypeIn'] = [
            'type',
            'in',
            'range' => RateTypes::getConstantsByName(),
        ];
        $rules['Unique'] = [
            ['currency_id_in', 'currency_id_out', 'type', 'created_at'],
            'unique',
            'targetAttribute' => ['currency_id_in', 'currency_id_out', 'type', 'created_at'],
        ];

        return $rules;
    }

    /**
     * @param Currencies $baseCurrency
     * @param Currencies $quoteCurrency
     * @param            $type
     * @param            $source
     *
     * @return float|int|mixed
     * @throws InvalidConfigException
     */
    public static function getFxRate(Currencies $baseCurrency, Currencies $quoteCurrency, $type, $source, $time)
    {
        $rate = FxRates::find()
            ->andWhere([
                '<',
                'fx_rates.created_at',
                $time,
            ])
            ->andWhere([
                'type'            => $type,
                'source'          => $source,
                'currency_id_in'  => $baseCurrency->getId(),
                'currency_id_out' => $quoteCurrency->getId(),
            ])
            ->orderBy(['fx_rates.created_at' => SORT_DESC])
            ->cache(60)
            ->one();

        if (null !== $rate) {
            return $rate->rate;
        }

        return 1;
    }

    /**
     * @param int $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @param int $currency_id_in
     */
    public function setCurrencyIdIn(int $currency_id_in)
    {
        $this->currency_id_in = $currency_id_in;
    }

    /**
     * @param int $currency_id_out
     */
    public function setCurrencyIdOut(int $currency_id_out)
    {
        $this->currency_id_out = $currency_id_out;
    }

    /**
     * @param int $deleted_at
     */
    public function setDeletedAt($deleted_at)
    {
        $this->deleted_at = $deleted_at;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param float $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @param int $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param int $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }
}