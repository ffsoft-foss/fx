<?php

namespace ffsoft\fx\drivers;

use common\models\Currencies;
use common\models\enums\RateTypes;
use CurrencyApi\CurrencyApi\CurrencyApiClient;
use CurrencyApi\CurrencyApi\CurrencyApiException;
use ffsoft\fx\enums\FxRatesSource;
use ffsoft\fx\exceptions\ParseException;
use ffsoft\fx\models\FxRates;
use Yii;
use yii\base\Exception;

/**
 * Class CurrencyApi
 *
 * @package ffsoft\fx\drivers
 */
class CurrencyApi implements RatesInterface
{
    /**
     * @param Currencies $currencyIn
     * @param Currencies $currencyOut
     * @param int        $type
     * @param int        $timestamp
     *
     * @return FxRates|object|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getRate(Currencies $currencyIn, Currencies $currencyOut, int $type, int $timestamp)
    {
        $date = date('d.m.Y-H.i', $timestamp);

        $baseCurrency = $type == RateTypes::BUY ? $currencyOut : $currencyIn;

        $key = [$date, __METHOD__, $baseCurrency->getCode()];
        $cache = Yii::$app->cache->get($key);

        if ($cache === false) {
            $cache = $this->parseData($baseCurrency);
            Yii::$app->cache->set($key, $cache, 60);
        }

        $rate = $this->fetchData($cache, $currencyIn->getCode(), $currencyOut->getCode(), $type);

        if (!empty($rate)) {
            $fxRate = Yii::createObject(
                [
                    'class'           => FxRates::class,
                    'currency_id_in'  => $currencyIn->getId(),
                    'currency_id_out' => $currencyOut->getId(),
                    'source'          => $this->getSource(),
                    'rate'            => $rate['rate'],
                    'type'            => $rate['type'],
                    'created_at'      => $rate['created_at'],
                ]
            );

            return $fxRate;
        }

        return null;
    }

    /**
     * @return int
     */
    public function getSource()
    {
        return FxRatesSource::CURRENCY_API;
    }

    /**
     * @param array  $data
     * @param string $currency_in
     * @param string $currency_out
     * @param int    $type
     *
     * @return array|null
     */
    private function fetchData(array $data, string $currency_in, string $currency_out, int $type)
    {
        foreach ($data as $rate) {
            if ($currency_in == $rate['currency_in']
                && $currency_out == $rate['currency_out']
                && $type == $rate['type']
            ) {
                return $rate;
            }
        }

        return null;
    }

    public function getApiKey()
    {
        return Yii::$app->getModule('fx')->getApiKey();
    }

    /**
     * @return array
     */
    private function parseData(Currencies $baseCurrency)
    {
        $currencyapi = new CurrencyApiClient($this->getApiKey());
        $data = [];

        try {

            $response = $currencyapi->latest(['base_currency' => $baseCurrency->getCode(), 'type' => 'fiat']);

            if (!isset($response['meta']['last_updated_at']) || !isset($response['data'])) {
                Yii::error(new ParseException(['response' => $response]));
                return $data;
            }

            $lastUpdatedAt = $response['meta']['last_updated_at'];
            $currencyRates = $response['data'];
            $timestamp = strtotime($lastUpdatedAt);

            foreach ($currencyRates as $quoteCurrencyCode => $quoteCurrency) {
                if ($baseCurrency->getCode() !== $quoteCurrencyCode) {

                    if (!isset($quoteCurrency['value'])) {
                        continue;
                    }

                    $data[] = [
                        'currency_in'  => $baseCurrency->getCode(),
                        'currency_out' => $quoteCurrencyCode,
                        'rate'         => $quoteCurrency['value'],
                        'type'         => RateTypes::SELL,
                        'created_at'   => $timestamp,
                    ];

                    $data[] = [
                        'currency_in'  => $quoteCurrencyCode,
                        'currency_out' => $baseCurrency->getCode(),
                        'rate'         => $quoteCurrency['value'],
                        'type'         => RateTypes::BUY,
                        'created_at'   => $timestamp,
                    ];
                }
            }
        } catch (CurrencyApiException $e) {
            Yii::error($e);
        }

        return $data;
    }
}
